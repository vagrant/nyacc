# Makefile.in for nyacc
#
# Copyright (C) 2017-2019 Matthew R. Wette
#
# Copying and distribution of this file, with or without modification
# is granted.  This file is offered as-is, without any warranty.

GUILE = @GUILE@
GUILD = @GUILD@

SITE_SCM_DIR = @GUILE_SITE@
SITE_SCM_GO_DIR = @GUILE_SITE_GO@

SHELL = @SHELL@
TOPDIR = @abs_top_builddir@

.PHONY: default
default: 
	@(cd module; make DESTDIR=$(DESTDIR))

.PHONY: install
install:
	@(cd module; make DESTDIR=$(DESTDIR) install)
	@(cd doc/nyacc; make DESTDIR=$(DESTDIR) install)

.PHONY: uninstall
uninstall:
	@echo "not smart enough to uninstall"

.PHONY: install-srcs
install-srcs:
	@(cd module; make DESTDIR=$(DESTDIR) install-srcs)

.PHONY: install-ffi-help
install-ffi-help:
	@echo "FFI helper now part of normal distribution"

.PHONY: dist-files
dist-files:
	(cd etc; autoconf -o ../configure)
	(cd module; make DESTDIR=$(DESTDIR) dist-files)
	(cd doc/nyacc; make DESTDIR=$(DESTDIR) dist-files)
	(cd examples; make DESTDIR=$(DESTDIR) dist-files)

.PHONY: check
check:
	(cd test-suite/nyacc; make DESTDIR=$(DESTDIR) check)

.PHONY: distcheck
distcheck:
	@echo "nothing to distcheck"

.PHONY: clean
clean:
	(cd module; make DESTDIR=$(DESTDIR) clean)

.PHONY: dist-clean
distclean:
	rm -f Makefile config.log config.status
	rm -f module/Makefile doc/nyacc/Makefile examples/Makefile
	rm -f test-suite/nyacc/Makefile
	rm -f test-suite/nyacc/lang/Makefile
	rm -f test-suite/nyacc/lang/c99/Makefile

.PHONY: install-nx-languages \
	install-nx-javascript \
	install-nx-octave \
	install-nx-tcl
install-nx-languages \
	install-nx-javascript \
	install-nx-octave \
	install-nx-tcl \
	:
	@(cd examples; \
	  make -f Makefile.nyacc \
		SITE_SCM_DIR=$(SITE_SCM_DIR) \
		SITE_SCM_GO_DIR=$(SITE_SCM_GO_DIR) \
		DESTDIR=$(DESTDIR) install-nx-utils $@)

# --- last line ---
